from django.contrib import admin
from django.urls import path, include

from core import views

urlpatterns = [

    path('', views.exibir_index, name = 'index'),
    path('', include('usuario.urls')),
    path('admin/', admin.site.urls),
    path('busway/', views.exibir_home, name = 'busway'),
    path('busway/rotas/', include('rotas.urls')),
    path('busway/', include('cadastros.urls')),
    path('busway/feed/', views.exibir_avisos, name = 'avisos'),
    path('busway/feed/aviso/novo', views.cadastrar_aviso, name = 'add_aviso'),
    path('busway/cadastros/', views.exibir_cadastros, name='cadastros'),
]
