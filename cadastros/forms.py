from django import forms

class CriarCategoriaForm(forms.Form):

    descricao = forms.CharField(required=True)


class CriarItemVeiculoForm(forms.Form):

    descricao = forms.CharField(required=True)
    categoria = forms.IntegerField(required=True)


class CriarPeriodoForm(forms.Form):

    descricao = forms.CharField(required=True)


class CriarAnaliseVeicular(forms.Form):

    itens = forms.CharField(max_length=1024)

