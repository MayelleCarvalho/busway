import json
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect

from cadastros.forms import CriarCategoriaForm, CriarItemVeiculoForm, CriarPeriodoForm
from core.forms import CriarVeiculoForm
from core.models import Perfil, Veiculo, Categoria_item_veiculo, Item_veiculo, Periodo, Avaliacao_veiculo, \
    Item_avaliacao


# Create your views here.
@login_required()
def exibir_veiculos(request):
    veiculos = Veiculo.objects.all()
    perfil = Perfil.objects.get(usuario_id = request.user.id)

    return render(request, "list_veiculos.html", {'veiculos':veiculos, 'perfil': perfil})


@login_required()
def cadastrar_veiculo(request):

    if request.method == "POST":
        form = CriarVeiculoForm(request.POST)
        if form.is_valid():
            dados = form.data
            veiculo = Veiculo(
                modelo = dados['modelo'],
                placa = dados['placa']
            )
            veiculo.save()
            return redirect('/busway/veiculos/')
    else:
        form = CriarVeiculoForm()
        perfil = Perfil.objects.get(usuario_id=request.user.id)
    return render(request, 'list_veiculos.html', {'form': form, 'perfil': perfil})


@login_required()
def excluir_veiculo(request, veiculo_id):
    veiculo = Veiculo.objects.get(pk=veiculo_id)
    veiculo.delete()
    return redirect('/busway/veiculos/')



@login_required()
def exibir_categorias(request):
    categorias = Categoria_item_veiculo.objects.all()
    perfil = Perfil.objects.get(usuario_id = request.user.id)

    return render(request, "categorias.html", {'categorias':categorias, 'perfil': perfil})


@login_required()
def cadastrar_categoria(request):

    if request.method == "POST":
        form = CriarCategoriaForm(request.POST)
        if form.is_valid():
            dados = form.data
            categoria = Categoria_item_veiculo(
                descricao = dados['descricao']
            )
            categoria.save()
            return redirect('/busway/categorias/')
    else:
        form = CriarCategoriaForm()
        perfil = Perfil.objects.get(usuario_id=request.user.id)
    return render(request, 'categorias.html', {'form': form, 'perfil': perfil})


@login_required()
def excluir_categoria(request, categoria_id):
    categoria = Categoria_item_veiculo.objects.get(pk=categoria_id)
    categoria.delete()
    return redirect('/busway/categorias/')


@login_required()
def exibir_itens_veiculo(request):
    itens_veiculo = Item_veiculo.objects.all()
    perfil = Perfil.objects.get(usuario_id = request.user.id)
    categorias = Categoria_item_veiculo.objects.all()

    return render(request, "itens_veiculo.html", {'categorias': categorias, 'itens_veiculo':itens_veiculo, 'perfil': perfil})


@login_required()
def cadastrar_item_veiculo(request):

    if request.method == "POST":
        form = CriarItemVeiculoForm(request.POST)
        if form.is_valid():
            dados = form.data
            item_veiculo = Item_veiculo(
                descricao = dados['descricao'],
                categoria_id = dados['categoria']
            )
            item_veiculo.save()
            return redirect('/busway/itens-veiculo/')
    else:
        categorias = Categoria_item_veiculo.objects.all()
        form = CriarItemVeiculoForm()
        perfil = Perfil.objects.get(usuario_id=request.user.id)
    return render(request, 'itens_veiculo.html', {'form': form, 'perfil': perfil, 'categorias': categorias})


@login_required()
def excluir_item_veiculo(request, item_veiculo_id):
    item_veiculo = Item_veiculo.objects.get(pk=item_veiculo_id)
    item_veiculo.delete()
    return redirect('/busway/itens-veiculo/')



@login_required()
def exibir_periodos(request):
    periodos = Periodo.objects.all()
    perfil = Perfil.objects.get(usuario_id = request.user.id)

    return render(request, "periodos.html", {'periodos':periodos, 'perfil': perfil})


@login_required()
def cadastrar_periodo(request):

    if request.method == "POST":
        form = CriarPeriodoForm(request.POST)
        if form.is_valid():
            dados = form.data
            periodo = Periodo(
                descricao = dados['descricao']
            )
            periodo.save()
            return redirect('/busway/periodos/')
    else:
        form = CriarPeriodoForm()
        perfil = Perfil.objects.get(usuario_id=request.user.id)
    return render(request, 'periodos.html', {'form': form, 'perfil': perfil})


@login_required()
def excluir_periodo(request, periodo_id):
    periodo = Periodo.objects.get(pk=periodo_id)
    periodo.delete()
    return redirect('/busway/periodos/')


@login_required()
def exibir_consulta_analise(request):
    perfil = Perfil.objects.get(usuario=request.user)
    return render(request, "consulta_analise_veicular.html", {'perfil': perfil})

@login_required()
def adicionar_analise_veicular(request):

    veiculos = Veiculo.objects.all()
    categorias = Categoria_item_veiculo.objects.all()
    itens_veiculo = Item_veiculo.objects.all()
    periodos = Periodo.objects.all()
    perfil = Perfil.objects.get(usuario=request.user)
    return render(request, "add_analise.html",
                  {'perfil': perfil ,
                   'periodos': periodos,
                   'veiculos':veiculos,
                   'categorias': categorias,
                   'itens_veiculo': itens_veiculo})
#
# def recebe_ajax(request):
#
#     minha_lista = request.POST.getlist('itens[]')
#     perfil = Perfil.objects.get(usuario=request.user)
#     print(minha_lista)
#     return render(request, "consulta_analise_veicular.html", {'perfil': perfil})

@login_required()
def create_analise(request):
    veiculos = Veiculo.objects.all()
    categorias = Categoria_item_veiculo.objects.all()
    itens_veiculo = Item_veiculo.objects.all()
    periodos = Periodo.objects.all()
    perfil = Perfil.objects.get(usuario=request.user)
    response_data = {}
    if request.POST.get('action') == 'post':

        veiculo_id = request.POST.get('veiculo')
        periodo_id = request.POST.get('periodo')
        itens = request.POST.get('itens')
        relato = request.POST.get('relato')

        response_data['veiculo'] = veiculo_id
        response_data['periodo'] = periodo_id
        response_data['relato'] = relato
        response_data['itens'] = itens
        print(itens)

        avaliacao =  Avaliacao_veiculo(
            veiculo_id = veiculo_id,
            emissor_id = perfil.id,
            relato = relato,
            periodo_id = periodo_id
        )
        avaliacao.save()

        for item in eval(itens):
            item_veiculo = Item_veiculo.objects.get(descricao=item['descricao'])
            item_avaliacao = Item_avaliacao(
                item_id = item_veiculo.id,
                estado = item['estado'],
                avaliacao_id = avaliacao.id
            )
            item_avaliacao.save()

        return JsonResponse(response_data)

    return render(request, "add_analise.html",
                  {'perfil': perfil,
                   'periodos': periodos,
                   'veiculos': veiculos,
                   'categorias': categorias,
                   'itens_veiculo': itens_veiculo})
