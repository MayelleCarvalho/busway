from django.urls import path

from cadastros import views

urlpatterns = [

    path('veiculos/', views.exibir_veiculos, name = 'veiculos'),
    path('veiculos/novo/',  views.cadastrar_veiculo, name = 'add_veiculo'),
    path('veiculos/delete/<int:veiculo_id>', views.excluir_veiculo, name='delete_veiculo'),
    path('categorias/', views.exibir_categorias, name = 'categorias'),
    path('categorias/novo/',  views.cadastrar_categoria, name = 'add_categoria'),
    path('categorias/delete/<int:categoria_id>', views.excluir_categoria, name='delete_categoria'),
    path('itens-veiculo/', views.exibir_itens_veiculo, name='itens_veiculo'),
    path('itens-veiculo/novo/', views.cadastrar_item_veiculo, name='add_item_veiculo'),
    path('itens-veiculo/delete/<int:item_veiculo_id>', views.excluir_item_veiculo, name='delete_item_veiculo'),
    path('periodos/', views.exibir_periodos, name='periodos'),
    path('periodos/novo/', views.cadastrar_periodo, name='add_periodo'),
    path('periodos/delete/<int:periodo_id>', views.excluir_periodo, name='delete_periodo'),
    path('analises/consulta/', views.exibir_consulta_analise, name='consulta_analise'),
    # path('analises/novo/', views.adicionar_analise_veicular, name='add_analise'),
    path('analises/novo/', views.create_analise, name='add_analise'),
]