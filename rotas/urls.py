from django.urls import path

from rotas import views

urlpatterns = [

    path('', views.exibir_rotas, name='rotas'),
    path('ativas/', views.exibir_rotas_ativas, name='rotas_ativas'),
    path('novo/', views.CadastraRotaView.as_view(), name='add_rota'),
    path('edit/<int:rota_id>', views.AddParadaRotaView.as_view(), name='add_paradas'),
    path('detail/<int:rota_id>', views.exibir_rota, name='detail_rota'),
    path('desativar/<int:rota_id>', views.desativar_rota, name='desativar_rota'),
    path('ativar/<int:rota_id>', views.ativar_rota, name='ativar_rota'),
]