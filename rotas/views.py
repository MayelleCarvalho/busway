import json
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views import View
from core.forms import CriarRotaForm, AddParadaRotaForm
from core.models import Perfil, Parada, Localizacao, Rota
import googlemaps
from dict_to_obj import DictToObj

# Create your views here.

@login_required()
def exibir_rotas(request):
    rotas = Rota.objects.all()
    perfil = Perfil.objects.get(usuario_id=request.user.id)
    return render(request, 'rotas.html', {'perfil': perfil, 'rotas': rotas})


@login_required()
def exibir_rotas_ativas(request):
    rotas = Rota.objects.filter(is_ativa = True)
    perfil = Perfil.objects.get(usuario_id=request.user.id)
    return render(request, 'rotas_ativas.html', {'perfil': perfil, 'rotas': rotas})


class CadastraRotaView(View):

    def get(self, request):
        perfil = Perfil.objects.get(usuario_id=request.user.id)
        return render(request, 'cadastro_rotas.html', {'perfil': perfil})

    def post(self, request):

        gmaps = googlemaps.Client(key='AIzaSyB8CMxmKVxteFM27Fs0qm7OKOa5oltNS74')
        perfil = Perfil.objects.get(usuario=request.user)
        form = CriarRotaForm(request.POST)
        if form.is_valid():
            dados = form.data
            rota = Rota(
                descricao = dados['descricao'],
                hora_inicio = dados['hora_inicio'],
                duracao_media = dados['duracao_media']
            )
            rota.save()
            localizacoes = request.POST.getlist('paradas')
            for item in localizacoes:

                lat = item.split(',')[0][1:len(item.split(',')[0])]
                lng = item.split(',')[1][1:-1]
                localizacao = Localizacao(
                    descricao = DictToObj(gmaps.reverse_geocode((lat, lng))[0]).__dict__.__getitem__('formatted_address'),
                    latitude = lat,
                    longitude= lng
                )
                localizacao.save()
                rota.localizacoes.add(localizacao)
            rota.save()
            return redirect('/busway/rotas/edit/%s' %rota.id )
        else:
            form = CriarRotaForm
        return render(request, 'cadastro_rotas.html', {'form': form, 'perfil': perfil})


class AddParadaRotaView(View):

    def get(self, request, rota_id):
        perfil = Perfil.objects.get(usuario_id=request.user.id)
        rota = Rota.objects.get(pk=rota_id)
        address_list = []
        for local in rota.get_localizacoes():
            latlng = '%s , %s' % (local.latitude, local.longitude)
            address_list.append(latlng)

        json_address_list = json.dumps(address_list)

        return render(request, 'add_paradas.html', {'perfil': perfil, 'rota': rota , 'address_list': json_address_list})

    def post(self, request, rota_id):

        gmaps = googlemaps.Client(key='AIzaSyB8CMxmKVxteFM27Fs0qm7OKOa5oltNS74')
        perfil = Perfil.objects.get(usuario=request.user)
        rota = Rota.objects.get(pk=rota_id)
        form = AddParadaRotaForm(request.POST)
        if form.is_valid():
            localizacoes = request.POST.getlist('paradas')
            for item in localizacoes:
                lat = item.split(',')[0][1:len(item.split(',')[0])]
                lng = item.split(',')[1][1:-1]
                localizacao = Localizacao(
                    descricao=DictToObj(gmaps.reverse_geocode((lat, lng))[0]).__dict__.__getitem__('formatted_address'),
                    latitude=lat,
                    longitude=lng
                )
                localizacao.save()
                parada = Parada(
                    localizacao_id=localizacao.id,
                    descricao=DictToObj(gmaps.reverse_geocode((lat, lng))[0]).__dict__.__getitem__('formatted_address')
                )
                parada.save()
                rota.paradas.add(parada)
            rota.is_ativa = True
            rota.save()
            return redirect('/busway/rotas/detail/%s' %rota_id)
        else:
            form = AddParadaRotaForm()
            localizacoes = rota.get_localizacoes()
            localizacoes_str = []
            for local in localizacoes:
                localizacoes_str.append(str(local))

        return render(request, 'add_paradas.html', {'perfil': perfil, 'form': form, 'rota': rota, 'localizacoes_str': localizacoes_str})


@login_required()
def excluir_rota(request, rota_id):
    rota = Rota.objects.get(pk=rota_id)
    for local in rota.localizacoes.all():
        local.delete()

    for parada in rota.paradas.all():
        local = parada.localizacao
        parada.delete()
        local.delete()

    rota.delete()

    return redirect('/busway/rotas/')


@login_required()
def ativar_rota(request, rota_id):

    rota = Rota.objects.get(pk=rota_id)
    rota.is_ativa = True
    rota.save()
    return redirect('/busway/rotas/')


@login_required()
def desativar_rota(request, rota_id):

    rota = Rota.objects.get(pk=rota_id)
    rota.is_ativa = False
    rota.save()
    return redirect('/busway/rotas/')


@login_required()
def exibir_rota(request, rota_id):

    rota = Rota.objects.get(pk=rota_id)
    perfil = Perfil.objects.get(usuario=request.user)
    localizacoes_rota_list = []
    paradas_rota_list = []
    for local in rota.get_localizacoes():
        latlng = '%s , %s' % (local.latitude, local.longitude)
        localizacoes_rota_list.append(latlng)

    json_localizacoes_rota_list = json.dumps(localizacoes_rota_list)
    for parada in rota.get_paradas():
        latlng = '%s , %s' % (parada.localizacao.latitude, parada.localizacao.longitude)
        paradas_rota_list.append(latlng)

    json_paradas_rota_list = json.dumps(paradas_rota_list)

    return render(request, 'rota_detail.html', {'perfil': perfil,
                                                'rota': rota,
                                                'json_localizacoes_rota_list': json_localizacoes_rota_list,
                                                'json_paradas_rota_list': json_paradas_rota_list})

