import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from core.forms import CriarAvisoForm
from core.models import Perfil, Veiculo, Aviso, Rota


def exibir_index(request):
    return render(request, "index.html")


@login_required()
def exibir_home(request):
    rota = Rota.objects.get(pk=1)
    perfil = Perfil.objects.get(usuario=request.user)
    localizacoes_rota_list = []
    paradas_rota_list = []
    for local in rota.get_localizacoes():
        latlng = '%s , %s' % (local.latitude, local.longitude)
        localizacoes_rota_list.append(latlng)

    json_localizacoes_rota_list = json.dumps(localizacoes_rota_list)
    for parada in rota.get_paradas():
        latlng = '%s , %s' % (parada.localizacao.latitude, parada.localizacao.longitude)
        paradas_rota_list.append(latlng)

    json_paradas_rota_list = json.dumps(paradas_rota_list)

    return render(request, 'home.html', {'perfil': perfil,
                                                'rota': rota,
                                                'json_localizacoes_rota_list': json_localizacoes_rota_list,
                                                'json_paradas_rota_list': json_paradas_rota_list})




@login_required()
def exibir_avisos(request):
    avisos = Aviso.objects.all()
    perfil = Perfil.objects.get(usuario_id = request.user.id)

    return render(request, "feed_avisos.html", {'avisos':avisos, 'perfil': perfil})


@login_required()
def cadastrar_aviso(request):
    perfil = Perfil.objects.get(usuario_id=request.user.id)
    if request.method == "POST":
        form = CriarAvisoForm(request.POST)
        if form.is_valid():
            dados = form.data
            aviso = Aviso(
                titulo = dados['titulo'],
                conteudo = dados['conteudo'],
                autor_id = perfil.id
            )
            aviso.save()
            return redirect('/busway/feed/')
    else:
        form = CriarAvisoForm()
        perfil = Perfil.objects.get(usuario_id=request.user.id)
    return render(request, 'feed_avisos.html', {'form': form, 'perfil': perfil})


def exibir_cadastros(request):
    perfil = Perfil.objects.get(usuario=request.user)
    return render(request, "cadastros.html", {'perfil': perfil})
