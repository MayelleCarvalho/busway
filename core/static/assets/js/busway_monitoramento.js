var map, infoWindow;
//var latitude = -5.101793;
//var longitude = -42.813250;

function initMap() {

    map = new google.maps.Map(document.getElementById('localizacao'), {
        zoom: 16,
        center: coordenadas,
        disableDefaultUI: true
    });
    
    infoWindow = new google.maps.InfoWindow;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        infoWindow.setPosition(pos);
        infoWindow.setContent('Localização teste.');
        infoWindow.open(map);
        map.setCenter(pos);

        var marker = new google.maps.Marker({
            position: {lat: position.coords.latitude, lng: position.coords.longitude },
            title: 'minha localização',
            map: map
        });

      }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
      });
    }
    else {
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}