var map, infoWindow, firebase, db, coordenadas;
var geocoder;
var map;
var list_paradas = [];
var cont = 0;
var flightPath;
var posAnterior = null;
var posPosterior = null;

function initMonitoramento() {

    map = new google.maps.Map(document.getElementById('mapLocalizacao'), {
        zoom: 14,
        center: {lat: -5.0950, lng: -42.809590},
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    });

    var markerPosOnibus = new google.maps.Marker({
        position: {lat: -5.0950, lng: -42.809590},
        title: "meu ônibus",
        map: map
    });

    firebase.initializeApp({
        apiKey: 'AIzaSyAfcWX8SRkbRvipahKjsrB_wzJOZwAJobs',
        authDomain: "ifpibusway.firebaseapp.com",
        projectId: "ifpibusway"
    });

    db = firebase.firestore();

    db.collection("localizacoes").doc("local01")
        .onSnapshot(function(doc){

            coordenadas = {
                    lat: doc.data()['local']['latitude'],
                    lng: doc.data()['local']['longitude']
            };
//            document.getElementById('inputLocal').value = doc.data()['local']['latitude'];
            posPosterior = new google.maps.LatLng(coordenadas);
            console.log(coordenadas);

            markerPosOnibus.setPosition(coordenadas);
            map.panTo(coordenadas);

//            markerPosOnibus.animateTo(coordenadas,{
//                duration: 13000,
//                complete: function() {
//                    alert("Chegou ao destino!");
//                }
//            });

    });

    var path_localizacoes = [];
    var locais = JSON.parse(document.getElementById("objetosId").getAttribute("data-localizacoes"));
    var paradas = JSON.parse(document.getElementById("objetosId").getAttribute("data-paradas"));

    for (i = 0; i < locais.length; i++) {

        var latlngStr = locais[i].split(",", 2);
        path_localizacoes.push(new google.maps.LatLng(parseFloat(latlngStr[0]), parseFloat(latlngStr[1])));

    }

    var iconParada = {
        url: document.getElementById("objetosId").getAttribute("data-icon"),
        scaledSize: new google.maps.Size(25, 25),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(0, 0)
    };

    for (i = 0; i < paradas.length; i++) {

        var latlngStr = paradas[i].split(",", 2);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(latlngStr[0]), parseFloat(latlngStr[1])),
            title: '#' + paradas[i],
            icon: iconParada,
            map: map
        });
    }

    console.log("testando");
    flightPath = new google.maps.Polyline({
        path: path_localizacoes,
        strokeColor: '#000000',
        strokeOpacity: 0.8,
        strokeWeight: 3
    });

    flightPath.setMap(map);
}
