var poly;
var map;
var list_paradas = [];
var cont = 0;

function initMap() {
    map = new google.maps.Map(document.getElementById('mapSetLocalizacao'), {
        zoom: 17,
        center: {lat: -5.101793, lng: -42.813250},
        disableDefaultUI: true
    });

    poly = new google.maps.Polyline({
      strokeColor: '#FFD700',
      strokeOpacity: 0.8,
      strokeWeight: 10
    });

    poly.setMap(map);

    map.addListener('click', addLatLng);

}

function addLatLng(event) {
    var path = poly.getPath();

    path.push(event.latLng);

    var marker = new google.maps.Marker({
      position: event.latLng,
      title: '#' + path.getLength(),
      map: map
    });

    list_paradas.push(event.latLng);
    createOptionSelect(list_paradas[cont]);
    cont++;
}

function createOptionSelect(latLng) {
    var geocoder = new google.maps.Geocoder();
    var opt = document.createElement("option");
    opt.setAttribute("value", latLng);

    geocoder.geocode({'location': latLng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            opt.appendChild(document.createTextNode(results[0].formatted_address));
        }
        else{
            opt.appendChild(document.createTextNode(latLng));
        }
        opt.setAttribute("selected", "true");
        document.getElementById("selectLocalizacoes").appendChild(opt);
    });
}