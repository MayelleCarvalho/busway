var geocoder;
var map;
var list_paradas = [];
var cont = 0;
var flightPath;

function initMap() {

    map = new google.maps.Map(document.getElementById('mapSetParada'), {
        zoom: 16,
        center: {lat: -5.101793, lng: -42.813250},
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    });

    var path = [];
    var locais = JSON.parse(document.getElementById("mainId").getAttribute("data-rota"));

    for (i = 0; i < locais.length; i++) {

        var latlngStr = locais[i].split(",", 2);
        path.push(new google.maps.LatLng(parseFloat(latlngStr[0]), parseFloat(latlngStr[1])));

    }

    flightPath = new google.maps.Polyline({
        path: path,
        strokeColor: '#000000',
        strokeOpacity: 0.8,
        strokeWeight: 3
    });

    flightPath.setMap(map);
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
    });
}

function placeMarker(location) {

    var iconParada = {
    url: document.getElementById("mainId").getAttribute("data-icon"),
    scaledSize: new google.maps.Size(25, 25),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(0, 0)
};

    var marker = new google.maps.Marker({
        position: location,
        title: '#' + location,
        icon: iconParada,
        map: map
    });
    createOptionSelect(location);
}


function createOptionSelect(latLng) {
    geocoder = new google.maps.Geocoder();
    var opt = document.createElement("option");
    opt.setAttribute("value", latLng);

    geocoder.geocode({'location': latLng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            opt.appendChild(document.createTextNode(results[0].formatted_address));
        }
        else{
            opt.appendChild(document.createTextNode(latLng));
        }
        opt.setAttribute("selected", "true");
        document.getElementById("selectParadas").appendChild(opt);
    });
}


