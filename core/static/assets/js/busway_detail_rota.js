var geocoder;
var map;
var list_paradas = [];
var cont = 0;
var flightPath;

function initMap() {

    map = new google.maps.Map(document.getElementById('mapDetailRota'), {
        zoom: 16,
        center: {lat: -5.101793, lng: -42.813250},
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    });

    var path_localizacoes = [];
    var locais = JSON.parse(document.getElementById("objetosId").getAttribute("data-localizacoes"));

    var paradas = JSON.parse(document.getElementById("objetosId").getAttribute("data-paradas"));

    for (i = 0; i < locais.length; i++) {

        var latlngStr = locais[i].split(",", 2);
        path_localizacoes.push(new google.maps.LatLng(parseFloat(latlngStr[0]), parseFloat(latlngStr[1])));

    }

    var iconParada = {
        url: document.getElementById("objetosId").getAttribute("data-icon"),
        scaledSize: new google.maps.Size(30, 30),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(0, 0)
    };


    for (i = 0; i < paradas.length; i++) {

        var latlngStr = paradas[i].split(",", 2);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(latlngStr[0]), parseFloat(latlngStr[1])),
            title: '#' + paradas[i],
            icon: iconParada,
            map: map
        });
    }

    flightPath = new google.maps.Polyline({
        path: path_localizacoes,
        strokeColor: '#000000',
        strokeOpacity: 0.8,
        strokeWeight: 3
    });

    flightPath.setMap(map);
}
