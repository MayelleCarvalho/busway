from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

# Create your models here.

class Perfil(models.Model):

    TIPO_PERFIL_CHOICE = (
        ('ADMINISTRADOR', 'Administrador'),
        ('ALUNO', 'Aluno'),
        ('FISCAL', 'Fiscal'),
        ('MOTORISTA', 'Motorista')
    )

    usuario = models.OneToOneField(User, related_name='perfil', on_delete=models.CASCADE)
    tipo_perfil = models.CharField(max_length=25, choices=TIPO_PERFIL_CHOICE)


class Veiculo(models.Model):

    placa = models.CharField(max_length=10)
    modelo = models.CharField(max_length=150)


class Localizacao(models.Model):

    descricao = models.CharField(max_length=300)
    latitude = models.CharField(max_length=20)
    longitude = models.CharField(max_length=20)

    def __str__(self):
        return '{lat: %s, lng: %s}' %(self.latitude, self.longitude)


class Parada(models.Model):

    descricao = models.CharField(max_length=200)
    localizacao = models.ForeignKey(Localizacao, related_name='parada', on_delete=models.CASCADE)


class Rota(models.Model):

    descricao = models.CharField(max_length=200)
    hora_inicio = models.TimeField(default=timezone.now())
    duracao_media = models.IntegerField()
    is_ativa = models.BooleanField(default=False)
    localizacoes = models.ManyToManyField(Localizacao)
    paradas = models.ManyToManyField(Parada)

    def get_localizacoes(self):
        return self.localizacoes.all()

    def get_paradas(self):
        return self.paradas.all()


class Categoria_item_veiculo(models.Model):

    descricao = models.CharField(max_length=200)


class Periodo(models.Model):

    descricao = models.CharField(max_length=100)


class Item_veiculo(models.Model):

    descricao = models.CharField(max_length=200)
    categoria = models.ForeignKey(Categoria_item_veiculo, related_name="item_veiculo", on_delete=models.CASCADE)


class Avaliacao_veiculo(models.Model):

    # STATUS_CHOICE = (
    #     ('AVALIACAO FISCAL', 'Aguardando confirmação do Motorista'),
    #     ('AVALIACAO MOTORISTA', 'Aguardando confirmação do Fiscal'),
    #     ('CONSENTIMENTO MOTORISTA', 'Confirmado por Motorista'),
    #     ('CONSENTIMENTO FISCAL', 'Confirmado por Fiscal'),
    #     ('VISUALIZADO ADMINISTRADOR', 'Visualizado por Administrador')
    # )

    data = models.DateTimeField(default=datetime.now())
    veiculo = models.ForeignKey(Veiculo, related_name="avaliacao", on_delete=models.CASCADE)
    emissor = models.ForeignKey(Perfil, related_name="avaliacao_emissor", on_delete=models.CASCADE)
    # motorista = models.ForeignKey(Perfil, related_name="avaliacao_motorista", on_delete=models.CASCADE)
    relato = models.CharField(max_length=500)
    periodo = models.ForeignKey(Periodo, related_name="avaliacao", on_delete=models.CASCADE)
    # status = models.CharField(max_length=100, choices=STATUS_CHOICE)
    itens_avaliados = models.ManyToManyField(Item_veiculo, through='Item_avaliacao')


class Item_avaliacao(models.Model):

    ESTADO_CHOICE = (
        ('BOM', 'Bom'),
        ('REGULAR', 'Regular'),
        ('DANIFICADO', 'Danificado'),
        ('AUSENTE', 'Ausente')
    )

    item = models.ForeignKey(Item_veiculo, related_name='item_avaliacao', on_delete=models.CASCADE)
    avaliacao = models.ForeignKey(Avaliacao_veiculo, related_name='item_avaliacao', on_delete=models.CASCADE)
    estado = models.CharField(max_length=10, choices=ESTADO_CHOICE)


class Avaliacao_km(models.Model):

    data = models.DateTimeField(default=datetime.now())
    veiculo = models.ForeignKey(Veiculo, related_name='avaliacao_km', on_delete=models.CASCADE)
    motorista = models.ForeignKey(Perfil, related_name='avaliacao_km', on_delete=models.CASCADE)
    km_saida = models.FloatField()
    km_chegada = models.FloatField()
    catraca_saida = models.IntegerField()
    catraca_chegada = models.IntegerField()
    hora_saida = models.TimeField()
    hora_chegada = models.TimeField()
    rota = models.ForeignKey(Rota, related_name='avaliacao_km', on_delete=models.CASCADE)


class Aviso(models.Model):

    data = models.DateTimeField(default=datetime.now())
    titulo = models.CharField(max_length=100)
    conteudo = models.TextField()
    autor = models.ForeignKey(Perfil, related_name='aviso', on_delete=models.CASCADE)

    def publicar(self):
        self.data = datetime.now()
        self.save()
