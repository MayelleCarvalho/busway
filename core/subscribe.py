import paho.mqtt.client as mqtt
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore


def on_connect(client, userdata, flags, rc):

    if rc == 1:
        print("Conexão falhou")
    else:
        print("Conectado")
        client_MQTT.subscribe("topic/GPSBUSWAY0101/data/")

def on_message(client, userdata, msg):
    Dict = eval(msg.payload.decode("utf-8"))
    # gmaps = googlemaps.Client(key='AIzaSyB8CMxmKVxteFM27Fs0qm7OKOa5oltNS74')
    data = {
        u'descricao': u'Localização do ônibus agora',
        u'local': {
            u'latitude': Dict['latitude'],
            u'longitude': Dict['longitude']
        }
    }
    db.collection(u'localizacoes').document(u'local01').set(data)
    print(data)



## firebase bd:
cred = credentials.Certificate('./static/assets/ifpibusway-155b3def07bc.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

## MQTT client
client_MQTT = mqtt.Client()
client_MQTT.on_connect = on_connect
client_MQTT.on_message = on_message
client_MQTT.connect("broker.hivemq.com", 1883, 60)
client_MQTT.loop_forever()

