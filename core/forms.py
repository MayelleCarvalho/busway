from django import forms


class CriarPerfilForm(forms.Form):

    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    tipo_perfil = forms.CharField(required=True)
    username = forms.CharField(required=True)
    password1 = forms.PasswordInput()
    password2 = forms.PasswordInput()


class CriarVeiculoForm(forms.Form):

    placa = forms.CharField(required=True)
    modelo = forms.CharField(required=True)


class CriarAvisoForm(forms.Form):

    titulo = forms.CharField(required=True)
    conteudo = forms.CharField(required=True)

class CriarRotaForm(forms.Form):

    descricao = forms.CharField(required=True)
    duracao_media = forms.IntegerField(required=True)
    hora_inicio = forms.TimeField(required=True)
    paradas = forms.SelectMultiple()


class AddParadaRotaForm(forms.Form):

    # descricao = forms.CharField(required=True)
    paradas = forms.SelectMultiple()
