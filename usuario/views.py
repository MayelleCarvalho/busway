import json
from django.contrib import messages
from django.contrib.auth import authenticate, login as auth_login, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.contrib.auth.views import auth_logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm
from django.shortcuts import render, redirect
from django.utils import timezone
from django.views import View
from core.forms import CriarPerfilForm, CriarVeiculoForm, CriarAvisoForm, CriarRotaForm, AddParadaRotaForm
from core.models import Perfil, Veiculo, Aviso, Parada, Localizacao, Rota
import googlemaps
from dict_to_obj import DictToObj
import paho.mqtt.client as mqtt


# Create your views here.
from core.models import Perfil


def cadastrar_usuario(request):
    if request.method == "POST":
        form_usuario = UserCreationForm(request.POST)
        if form_usuario.is_valid():
            form_usuario.save()
            usuario = User.objects.get(username = form_usuario.data['username'])
            perfil = Perfil(tipo_perfil = 'ALUNO', usuario_id = usuario.id)
            perfil.save()
            return redirect('login')
    else:
        form_usuario = UserCreationForm()
    return render(request, 'cadastro.html', {'form_usuario': form_usuario})


def login(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        usuario = authenticate(request, username=username, password=password)
        if usuario is not None:
            auth_login(request, usuario)
            return redirect('busway')
        else:
            form_login = AuthenticationForm()
    else:
        form_login = AuthenticationForm()
    return render(request, 'login.html', {'form_login': form_login})


def logout(request):
    auth_logout(request)
    return redirect('login')


@login_required()
def editar_senha(request):
    perfil = Perfil.objects.get(usuario_id=request.user.id)
    if request.method == "POST":
        form_senha = PasswordChangeForm(request.user, request.POST)
        if form_senha.is_valid():
            user = form_senha.save()
            update_session_auth_hash(request, user)
            return redirect('busway')
    else:
        form_senha = PasswordChangeForm(request.user)
    return render(request, 'edit_senha_user.html', {'form_senha': form_senha, 'perfil': perfil})



class CadastraPerfilView(View):

    def get(self, request):
        perfil = Perfil.objects.get(usuario_id=request.user.id)
        return render(request, 'add_perfil.html', {'perfil':perfil})


    def post(self, request):
        form = CriarPerfilForm(request.POST)
        if form.is_valid():
            dados = form.data
            senha = make_password("%s" % dados['password1'])
            usuario = User(username=dados['username'],
                           first_name=dados['first_name'],
                           last_name=dados['last_name'],
                           email=dados['email'],
                           password=senha,
                           last_login=timezone.now(),
                           is_superuser=False,
                           is_staff=True,
                           is_active=True,
                           date_joined=timezone.now())
            usuario.save()

            perfil = Perfil(tipo_perfil=dados['tipo_perfil'],
                            usuario_id=usuario.id)

            perfil.save()
            messages.add_message(request, messages.SUCCESS, 'Perfil criado com sucesso!')
            return redirect('agentes')

        perfil = Perfil.objects.get(usuario_id=request.user.id)
        messages.add_message(request, messages.ERROR, 'Algo deu errado, preencha o formulário novamente.')

        return render(request, 'add_perfil.html', {'form': form , 'perfil':perfil})


@login_required()
def exibir_agentes(request):
    perfis = Perfil.objects.all()
    perfil = Perfil.objects.get(usuario_id = request.user.id)
    agentes = []
    for p in perfis:
        if(p.tipo_perfil != 'ALUNO'):
            agentes.append(p)
    agentes.remove(perfil)
    return render(request, "list_agentes.html", {'agentes':agentes, 'perfil': perfil})


@login_required()
def exibir_perfil(request):

    perfil = Perfil.objects.get(usuario_id = request.user.id)
    return render(request, "detail_perfil.html", {'perfil': perfil})


class EditaPerfilView(View):

    def get(self, request):
        perfil = Perfil.objects.get(usuario=request.user)
        return render(request, 'edit_perfil.html', {'perfil': perfil})

    def post(self, request):
        perfil = Perfil.objects.get(usuario=request.user)

        form = CriarPerfilForm(request.POST)
        print(form)
        if (form.is_valid()):
            dados = form.data

            usuario = User.objects.get(pk = perfil.usuario.id)
            usuario.email = dados['email']
            usuario.first_name = dados['first_name']
            usuario.last_name = dados['last_name']
            usuario.username = dados['username']
            usuario.save()

            perfil.tipo_perfil = dados['tipo_perfil']
            perfil.save()
            return redirect('busway')

        return render(request, 'edit_item.html',
                      {'perfil': perfil,'form': form})


@login_required()
def excluir_agente(request, agente_id):
    perfil = Perfil.objects.get(pk=agente_id)
    usuario = User.objects.get(pk=perfil.usuario.id)
    perfil.delete()
    usuario.delete()
    return redirect('/busway/agentes/')
