from django.urls import path

from usuario import views

urlpatterns = [

    path('user/novo/', views.cadastrar_usuario, name = 'add_user'),
    path('busway/perfil/novo/',  views.CadastraPerfilView.as_view(), name = 'add_perfil'),
    path('busway/perfil/detail/',  views.exibir_perfil, name = 'detail_perfil'),
    path('busway/perfil/edit/',  views.EditaPerfilView.as_view(), name = 'edit_perfil'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('busway/agentes/delete/<int:agente_id>', views.excluir_agente, name='delete_agente'),
    path('user/edit/senha', views.editar_senha, name='edit_senha'),
    path('busway/agentes/', views.exibir_agentes, name = 'agentes'),
]